# Exercise 1:
# Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах
# і задекоруйте ним основну функцію гри з попередньої дз.
# Після закінчення гри декоратор має сповістити, скільки тривала гра.
# Винесіть всі функції та декоратор в окремий файл та виконайте імпорт в основний

from time import time


def get_user_age():
    """
    This function checks input and gets an age from the user in format of a string

    Returns:
        (str): age of the user if str is a digit

    """

    while True:
        user_age = input("Будь ласка, введіть свій вік: ")

        if not user_age.isdigit():
            print("Будь ласка, введіть число повних років: ")
        else:
            return user_age


def define_word_for_years(user_age):
    """
    This function defines correct word to match the number of years entered by the user

    Args:
        user_age (str): age of the user returned by get_user_age() function

    Returns:
        (str): one of 'рік', 'роки' or 'років' to match the number of years

    """
    single_digit = {
        "0": "років",
        "1": "рік",
        "2": "роки",
        "3": "роки",
        "4": "роки",
        "5": "років",
        "6": "років",
        "7": "років",
        "8": "років",
        "9": "років"
    }
    double_digit = {
        "11": 'років',
        "12": 'років',
        "13": 'років',
        "14": 'років'
    }

    if user_age[-2:] in double_digit:
        word_for_years = double_digit[user_age[-2:]]
        return word_for_years
    else:
        word_for_years = single_digit[user_age[-1:]]
        return word_for_years


def respond_to_user(user_age, word_for_years):
    """
    This function generates a message for the user as a reply to entered age

    Args:
        user_age (str): age of the user returned by get_user_age() function
        word_for_years (str): correct word to match the number of years returned by define_word_for_years(user_age)

    Returns:
        (str): message for the user depending on the user's age

    """

    int_age = int(user_age)
    if 1 < len(user_age) == user_age.count(user_age[0]):
        msg = f"О, вам {int_age} {word_for_years}! Який цікавий вік!"
    elif int_age < 7:
        msg = f"Тобі ж {int_age} {word_for_years}! Де твої батьки?"
    elif int_age < 16:
        msg = f"Тобі лише {int_age} {word_for_years}, а це є фільм для дорослих!"
    elif int_age > 65:
        msg = f"Вам {int_age} {word_for_years}? Покажіть пенсійне посвідчення!"
    else:
        msg = f"Незважаючи на те, що вам {int_age} {word_for_years}, білетів все одно нема!"
    return msg


def exit_function():
    """
    This function checks if the user wants to continue

    Returns:
        (str): answer of the user which continues or terminates the program

    """
    answer = input(f"Спробуєте ще раз? Скажіть Yes або No: ")
    if answer == "Yes" or answer == "yes":
        return False
    elif answer == "No" or answer == "no":
        return True
    else:
        print(f"Спробуйте відповісти Yes або No наступного разу ")
        return False


def game_decorator(func):
    """
    This decorator function returns wrapping function which calculates the time of running the main game function
    Args:
        func: function which performs the main business logic

    Returns:
        wrapper function which calculates duration of the func execution
    """

    def wrapper(*f_args, **f_kwargs):
        start = time()
        res = func(*f_args, **f_kwargs)
        end = time()
        print(f"Гра тривала {(end - start):.2f} секунди")
        return res
    return wrapper


@game_decorator
def game():
    """
    This function gets the age of the user, delivers correct message to the user and allows to proceed or to stop

    """
    while True:
        user_age = get_user_age()
        word_for_years = define_word_for_years(user_age=user_age)
        msg = respond_to_user(user_age=user_age, word_for_years=word_for_years)
        print(msg)
        if exit_function():
            break




