# # Exercise 1
#
# # Напишіть цикл, який буде вимагати від користувача ввести слово,
# # в якому є буква "о" (враховуються як великі так і маленькі).
# # Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

letters_o = 'oOоО'  # latin and cyrillic
length_letters_o = 4
word_with_o = input('Please enter a word with letter o or O: ')
found_o = False
iterations = 0

while length_letters_o > iterations and not found_o:
    for i in letters_o:
        if i in word_with_o:
            found_o = True
            iterations += 1
            break
        else:
            iterations += 1
            if length_letters_o > iterations:
                continue
            else:
                word_with_o = input('Please enter a word with letter o or O: ')
                iterations = 0

# Exercise 2

# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = [i for i in lst1 if type(i) == str]
print(lst2)

# Exercise 3

# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о" (враховуються як великі так і маленькі).

text = input('Please enter a text: ')
count = 0
letters_o = 'oOоО'
words = text.split(' ')
for word in words:
    if word[-1] in letters_o:  # latin and cyrillic
        count += 1
    else:
        continue
print(f'Number of words ending with "o"/"O" is: {count}')