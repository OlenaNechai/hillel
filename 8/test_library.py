import library

import pytest

choices_return_kopecks_word = [
    (1, 'копійка'),
    (71, 'копійка'),
    (4, 'копійки'),
    (32, 'копійки'),
    (12, 'копійок'),
    (37, 'копійок')
]

choices_return_hryvna_word = [
    (21, 'гривня'),
    (1, 'гривня'),
    (3, 'гривні'),
    (62, 'гривні'),
    (59, 'гривень'),
    (11, 'гривень')
]

choices_return_hryvna_kopecks_word = [
    (31.30, ['31 гривня', '30 копійок']),
    (1.09, ['1 гривня', '09 копійок']),
    (82.43, ['82 гривні', '43 копійки']),
    (3.04, ['3 гривні', '04 копійки']),
    (100.01, ['100 гривень', '01 копійка']),
    (66.51, ['66 гривень', '51 копійка'])
]

choices_is_hot_today = [
    (35, True),
    (15, False),
    ('22', False)
]


@pytest.mark.parametrize('value,expected', choices_return_kopecks_word)
def test_return_kopecks_word(value, expected):
    assert library.return_kopecks_word(value) == expected
    result = library.return_kopecks_word(value)
    assert isinstance(result, str)

@pytest.mark.parametrize('value,expected', choices_return_hryvna_word)
def test_return_hryvna_word(value, expected):
    assert library.return_hryvna_word(value) == expected
    result = library.return_hryvna_word(value)
    assert isinstance(result, str)

@pytest.mark.parametrize('value,expected', choices_return_hryvna_kopecks_word)
def test_return_hryvna_kopecks_word(value, expected):
    assert library.return_hryvna_kopecks_word(value) == expected
    result = library.return_hryvna_kopecks_word(value)
    assert isinstance(result, list)

@pytest.mark.parametrize('value,expected', choices_is_hot_today)
def test_is_hot_today(value, expected):
    assert library.is_hot_today(value) == expected
    result = library.is_hot_today(value)
    assert isinstance(result, bool)
