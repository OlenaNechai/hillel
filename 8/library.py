# там, де явно прописані назви функцій та файлів - робіть у відповідності до ТЗ, в інших випадках помізкуйте щодо назв.
#
# в функціях проставляйте аннотацію типів аргументів та повертаємого значення, докстрінги,
# якщо функціонал коду функції зрозумілий з її назви - не потрібні
#
#
# створити файл library.py
# в ньому написати функції
# написати функцію, яка отримує ціле число і повертає слово "копійка" у вірній формі:
# 1 -- копійка, 2 -- копійки, 25 -- копійок

# написати функцію, яка отримує ціле число і повертає слово "гривня" у вірній формі:
# 1 -- гривня, 2 -- гривні, 25 -- гривень

# функцію, яка приймає число і повертає список,
# в якому перший елемент - стрічка з цілою частиною числа + слово 'гривня' у вірному відмінку,
# другий елемент - стрічка з мантісою (значення після коми), + слово 'копійка' у вірній формі.
# наприклад - 1 -- ['1 гривня', "0 копійок"]; 10.1 -- ['10 гривень', "10 копійок"]; 2.01 -- ['2 гривні', "1 копійка"].
# в даній функції для створення форм слів гривня та копійка використати результат роботи функцій, описаних вище.
# Зауваження - якщо буде передано число з дрібною частиною більш ніж 2 знаки, то вони не мають оброблятися
# (логіку, що робити з 125.339 залишаю на вас - чи округляйте, чи відкидайте - і це рішення пропишіть в докстрінгі)
#
# напишіть функцію is_hot_today, яка отримує параметр температури (число, за замовчуванням 30),
# і в залежності від величини повідомляє, чи сьогодні жарко, чи холодно (більше 25 - жарко, інакше холодно).
# перевірку на -155555555555 градусів чи +555555555555 не проводимо, просто відштовхуємося від отриманого значення.
# подумайте, який тип даних має повертати функція

# створіть функцію, яка отримує від користувача число і повертає його як число. проте!!!
# якщо користувач ввів не вірні дані, які не можна конвертувати в число ("шість"),
# заставте користувача ввести валідні дані (цикли вам в допомогу).
# результатом в будь-якому випадку має бути число.
# зауважте - функція має прийняти один не обовязковий стрічковий аргумент - месседж, за замовчуванням - "Введіть число"
#
#


from typing import Union


def return_kopecks_word(kopecks_num: int) -> str:
    if kopecks_num % 10 == 1 and kopecks_num != 11 and kopecks_num % 100 != 11:
        kopecks_word = 'копійка'
        return kopecks_word
    elif 1 < kopecks_num % 10 <= 4 and kopecks_num != 12 and kopecks_num != 13 and kopecks_num != 14:
        kopecks_word = 'копійки'
        return kopecks_word
    else:
        kopecks_word = 'копійок'
        return kopecks_word


def return_hryvna_word(hryvna_num: int) -> str:
    if hryvna_num % 10 == 1 and hryvna_num != 11 and hryvna_num % 100 != 11:
        hryvna_word = 'гривня'
        return hryvna_word
    elif 1 < hryvna_num % 10 <= 4 and hryvna_num != 12 and hryvna_num != 13 and hryvna_num != 14:
        hryvna_word = 'гривні'
        return hryvna_word
    else:
        hryvna_word = 'гривень'
        return hryvna_word


def return_hryvna_kopecks_word(user_input: float) -> list:
    """
    Takes user input of sum of money
    Returns: list with number of hryvnas with correct word for hryvna and number of kopecks with correct word for kopecks
    Only 2 first digits of decimal part are taken by slicing 2 first elements from kopecks string

    """
    hryvna_kopecks_list = []
    money = str(user_input)
    hrn = money.split('.')[0]
    if len(money.split('.')[1]) < 2:
        kopecks = money.split('.')[1][:] + '0'
    else:
        kopecks = money.split('.')[1][:2]
    hrn_word = return_hryvna_word(int(hrn))
    kpk_word = return_kopecks_word(int(kopecks))
    hryvna_kopecks_list.append(f'{hrn} {hrn_word}')
    hryvna_kopecks_list.append(f'{kopecks} {kpk_word}')
    return hryvna_kopecks_list


def is_hot_today(number: Union[int, float] = 30) -> bool:
    if type(number) is int:
        if number > 25:
            print(f'It\'s hot today! {number} degrees')
            return True
        else:
            print(f'It\'s not hot today! {number} degrees')
            return False
    else:
        print('Please enter integer next time')
        return False


def return_number_to_user(message: str = 'Введіть число') -> float:
    """

    Args:
        message: user can specify the message to be used to ask for the number

    Returns:
        integer to the user if input can be interpreted as a number

    """
    while True:
        try:
            user_input = float(input(f"{message}: "))
            return float(user_input)
        except:
            print('Наступного разу введіть число, наприклад 10.50')
            pass



