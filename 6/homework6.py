# Exercise 1
# Напишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
#
# Попросіть користувача ввести свсвій вік.
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
# - якщо вік користувача складається з однакових цифр \
# (11, 22, 44 і тд років, всі можливі варіанти!)- вивести "О, вам <>! Який цікавий вік!"
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
# Наприклад :
# "Тобі ж 5 років! Де твої батьки?"
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
# "О, вам 33 роки! Який цікавий вік!"
#
# Після отримання відповіді програма повинна спитати користувача, чи хоче він повторити виконання.
# Користувач відповідає Yes або No. У випадку коли користувач відповів Yes програма повторюється, No - завершується.
# Користувач повинен мати змогу повторити виконання програми необмежену кількість разів.
#
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг або тайпхінтінг.
# Не забувайте що кожна функція має виконувати тільки одне завдання і про правила написання коду.
#
# P.S. Для цієї і для всіх наступних домашок пишіть в функціях докстрінги або хінтінг


single_digit = {
    "0": "років",
    "1": "рік",
    "2": "роки",
    "3": "роки",
    "4": "роки",
    "5": "років",
    "6": "років",
    "7": "років",
    "8": "років",
    "9": "років"
}
double_digit = {
    "11": 'років',
    "12": 'років',
    "13": 'років',
    "14": 'років'
}


def get_user_age():
    """
    This function checks input and gets an age from the user in format of a string

    Returns:
        (str): age of the user if str is a digit

    """

    while True:
        user_age = input("Будь ласка, введіть свій вік: ")

        if not user_age.isdigit():
            print("Будь ласка, введіть число повних років: ")
        else:
            return user_age


def define_word_for_years(user_age):
    """
    This function defines correct word to match the number of years entered by the user

    Args:
        user_age (str): age of the user returned by get_user_age() function

    Returns:
        (str): one of 'рік', 'роки' or 'років' to match the number of years

    """
    if user_age[-2:] in double_digit:
        word_for_years = double_digit[user_age[-2:]]
        return word_for_years
    else:
        word_for_years = single_digit[user_age[-1:]]
        return word_for_years


def respond_to_user(user_age, word_for_years):
    """
    This function generates a message for the user as a reply to entered age

    Args:
        user_age (str): age of the user returned by get_user_age() function
        word_for_years (str): correct word to match the number of years returned by define_word_for_years(user_age)

    Returns:
        (str): message for the user depending on the user's age

    """

    int_age = int(user_age)
    if 1 < len(user_age) == user_age.count(user_age[0]):
        msg = f"О, вам {int_age} {word_for_years}! Який цікавий вік!"
    elif int_age < 7:
        msg = f"Тобі ж {int_age} {word_for_years}! Де твої батьки?"
    elif int_age < 16:
        msg = f"Тобі лише {int_age} {word_for_years}, а це є фільм для дорослих!"
    elif int_age > 65:
        msg = f"Вам {int_age} {word_for_years}? Покажіть пенсійне посвідчення!"
    else:
        msg = f"Незважаючи на те, що вам {int_age} {word_for_years}, білетів все одно нема!"
    return msg


def exit_function():
    """
    This function checks if the user wants to continue

    Returns:
        (str): answer of the user which continues or terminates the program

    """
    answer = input(f"Спробуєте ще раз? Скажіть Yes або No: ")
    if answer == "Yes" or answer == "yes":
        return False
    elif answer == "No" or answer == "no":
        return True
    else:
        print(f"Спробуйте відповісти Yes або No наступного разу ")
        return False


def game():
    """
    This function gets the age of the user, delivers correct message to the user and allows to proceed or to stop

    """
    while True:
        user_age = get_user_age()
        word_for_years = define_word_for_years(user_age=user_age)
        msg = respond_to_user(user_age=user_age, word_for_years=word_for_years)
        print(msg)
        if exit_function():
            break


game()


