# Exercise 1

first = 10
second = 30
print(first + second)
print(first - second)
print(first * second)
print(first / second)
print(first // second)
print(first % second)
print(first ** second)

# Exercise 2

result = first > second
print(result)

result = first >= second
print(result)

result = first < second
print(result)

result = first <= second
print(result)

result = first == second
print(result)

result = first != second
print(result)
